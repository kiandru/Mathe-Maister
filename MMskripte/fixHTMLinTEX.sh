#!/bin/sh

#set -e

sed -e "s/&lt;/</g" -e "s/&gt;/>/g" -e 's/<br\/>/\\\\/g' -e "s/&nbsp;/~/g"
