
- genutzte Anki-Version: 2.0.x auf Linuxmint und Windows 10
- beim Importieren nach Anki aufpassen, dass der Haken für HTML gesetzt wurde (über den Feldern) und der richtige Notiztyp und das richtige Deck ausgewählt wurden, und die Option mit "Update if first field matches"/"Karte aktualisieren, wenn erstes Feld gleich" oder so
- immer alles mit UTF-8 speichern, damit Umlaute und ß nicht verloren gehen
- genutzte Libre Office Version (Mint): 5.1.4.2
